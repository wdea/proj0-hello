# Proj0-Hello
-------------

Trivial project to exercise version control, turn-in, and other
mechanisms.

Author: Wally Dea
Contact: waldendea55@gmail.com
Desc: A simple script that prints Hello world. Testing git repos.
